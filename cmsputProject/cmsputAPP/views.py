
from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from .models import Contenido

formulario = """
No existe valor en la base de datos para esta llave. 
<p> Introdúcela:
<p>
<form action="" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""

@csrf_exempt
def get_content(request, llave):

    if request.method == 'POST': #Si el método de la petición es POST
        valor = request.POST.get('valor') #Extraemos los datos del cuerpo de HTTP
        c = Contenido(clave=llave, valor=valor) #Guardamos el contenido asociado a la clave
        c.save() # Guardo en la base de datos
    elif request.method == 'PUT': #Si el método de la petición es PUT
        valor = request.body.decode('utf-8')  # Obtener el valor del cuerpo de la solicitud
        try:
            contenido = Contenido.objects.get(clave=llave) #Se extrae el contenido asociado a esa clave
            contenido.valor = valor #Si se encuentra, su nuevo valor será el extraido de la solicitud HTTP 
        except Contenido.DoesNotExist: #Si no se encuentra
            contenido = Contenido(clave=llave, valor=valor) #Se crea un nuevo objeto
        contenido.save()
    else:
        valor = None
    try:
        contenido = Contenido.objects.get(clave=llave) #Si existe
        respuesta = "La llave " + contenido.clave + " tiene el valor " + contenido.valor + " y el id " + str(contenido.id)
    except Contenido.DoesNotExist: #Si no existe se envía un formulario para introducirlo
        respuesta = formulario

    return HttpResponse(respuesta)

def index(request):
    content_list = Contenido.objects.all() # Lista de todos los contenidos
    template = loader.get_template('cms/index.html') # Cargar la plantilla
    context = {			# Pasamos la lista de contenidos a la plantilla
        'content_list': content_list,
    }
    return HttpResponse(template.render(context, request))
